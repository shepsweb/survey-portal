<?php

namespace Sirs\SurveyPortal\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HomeController
{
    public function home(Request $request)
    {
        if ($url = Config::get('survey-portal.home')) {
            return redirect(portal_url($url));
        }

        return view('survey-portal::home');
    }
}
