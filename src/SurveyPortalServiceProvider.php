<?php

namespace Sirs\SurveyPortal;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\ServiceProvider;
use Sirs\SurveyPortal\Middleware\AuthenticateToken;

class SurveyPortalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'survey-portal');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'survey-portal');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadFactoriesFrom(__DIR__ . '/../database/factories');
        // $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/survey-portal.php' => config_path('survey-portal.php'),
            ], 'sirs-survey-portal-config');

            // Publishing the views.
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/survey-portal'),
            ], 'sirs-survey-portal-views');

            // Publishing assets.
            $this->publishes([
                __DIR__ . '/../resources/survey-portal-ui/dist/sirs-survey-portal.min.js' => public_path('vendor/sirs-survey-portal-ui/sirs-survey-portal.min.js'),
            ], 'sirs-survey-portal-assets');

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/survey-portal'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/survey-portal.php', 'survey-portal');

        $this->setProviderConfig();

        // Register the main class to use with the facade
        $this->app->singleton('survey-portal', function () {
            return new SurveyPortal;
        });

        $this->configureMiddleware();
    }

    /**
     * Set provider config
     *
     * @return void
     */
    protected function setProviderConfig(): void
    {
        $name = config('survey-portal.provider.name');

        config([
            "auth.providers.{$name}" => array_merge(
                config('survey-portal.provider.options'),
                config("auth.providers.{$name}", [])
            ),
        ]);
    }

    /**
     * Configure the SurveyPortal middleware and priority.
     *
     * @return void
     */
    protected function configureMiddleware(): void
    {
        $kernel = $this->app->make(Kernel::class);

        $kernel->pushMiddleware(AuthenticateToken::class)
            ->prependToMiddlewarePriority(AuthenticateToken::class);
    }
}
