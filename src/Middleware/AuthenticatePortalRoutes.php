<?php

namespace Sirs\SurveyPortal\Middleware;

use Closure;
use Illuminate\Http\Request;
use Sirs\SurveyPortal\Facades\SurveyPortal;
use Sirs\SurveyPortal\Exceptions\InvalidTokenException;

class AuthenticatePortalRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (SurveyPortal::canLoadPortalView($request->getRequestUri())) {
            return $next($request);
        }

        throw new InvalidTokenException;
    }
}
