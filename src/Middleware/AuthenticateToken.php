<?php

namespace Sirs\SurveyPortal\Middleware;

use Closure;
use Laravel\Sanctum\Guard;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Facades\Config;
use Sirs\SurveyPortal\Facades\SurveyPortal;
use Sirs\SurveyPortal\Exceptions\InvalidTokenException;

class AuthenticateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->allowAccess($request)) {
            return $next($request);
        }

        throw new InvalidTokenException;
    }

    /**
     * Allow access to the given request
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function allowAccess(Request $request): bool
    {
        if ((bool) $request->portal) {
            $referer = $request->header('Referer', null);
            return SurveyPortal::isValidReferer($referer) && $this->isAuthenticated($request);
        }

        return true;
    }

    /**
     * Check if authenticated
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    protected function isAuthenticated(Request $request): bool
    {
        if ($request->token) {
            $request->headers->set('Authorization', "Bearer {$request->token}");
        }

        $auth = resolve(AuthManager::class);

        $guard = new Guard(
            $auth,
            Config::get('survey-portal.expiration'),
            Config::get('survey-portal.provider.name')
        );

        SurveyPortal::setAuth($guard->__invoke($request));

        return SurveyPortal::canLoadPortalView($request->getRequestUri());
    }
}
