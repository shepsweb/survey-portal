<?php

namespace Sirs\SurveyPortal;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Sirs\SurveyPortal\Models\ApiUser;
use Illuminate\Support\Facades\Config;
use Sirs\SurveyPortal\Middleware\AuthenticatePortalRoutes;

class SurveyPortal
{
    protected $auth;

    /**
     * Set auth
     *
     * @param mixed $user
     * @return void
     */
    public function setAuth($user = null): void
    {
        $this->auth = null;

        if ($user !== null && get_class($user) === Config::get('survey-portal.provider.options.model', ApiUser::class)) {
            $this->auth = $user;
        }
    }

    /**
     * Get auth
     *
     * @return mixed
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * Can load portal view
     *
     * @param string|null $path
     * @return bool
     */
    public function canLoadPortalView(?string $path = null): bool
    {
        return $this->getAuth() !== null || $this->isAllowed($path);
    }

    /**
     * Register token manager routes
     *
     * @return void
     */
    public function tokenRoutes(): void
    {
        Route::namespace('\Sirs\SurveyPortal\Controllers')->group(function () {
            Route::get('/survey-portal/tokens', 'TokenManagerController@index')->name('survey-portal.tokens.index');
            Route::post('/survey-portal/tokens', 'TokenManagerController@store')->name('survey-portal.tokens.store');
            Route::delete('/survey-portal/tokens/{user}', 'TokenManagerController@destroy')->name('survey-portal.tokens.destroy');
        });
    }

    /**
     * Register portal routes
     *
     * @param \Closure|null $callback
     * @return void
     */
    public function portalRoutes(?Closure $callback = null): void
    {
        Route::middleware(AuthenticatePortalRoutes::class)->group(function () use ($callback) {
            Route::namespace('\Sirs\SurveyPortal\Controllers')->group(function () {
                Route::get('/survey-portal', 'HomeController@home')->name('survey-portal.home');
            });

            if ($callback !== null) {
                $callback();
            }
        });
    }

    /**
     * Check if given referer is valid
     *
     * @param string|null $referer
     * @return bool
     */
    public function isValidReferer(?string $referer): bool
    {
        return (bool) collect(Config::get('survey-portal.allowed.referers', []))
            ->prepend(Config::get('app.url'))
            ->first(function ($url) use ($referer) {
                return Str::startsWith($referer, $url);
            });
    }

    /**
     * Check if given url is allowed
     *
     * @param string $url
     * @return bool
     */
    public function isAllowed(string $url): bool
    {
        return (bool) collect(Config::get('survey-portal.allowed.paths', []))
            ->first(function ($path) use ($url) {
                return Str::is($path, $url);
            });
    }

    /**
     * Get abilities from config
     *
     * @return array
     */
    public function abilities(): array
    {
        return Config::get('survey-portal.abilities', []);
    }
}
