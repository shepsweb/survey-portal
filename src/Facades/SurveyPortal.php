<?php

namespace Sirs\SurveyPortal\Facades;

use Illuminate\Support\Facades\Facade;
use Sirs\SurveyPortal\Tests\Fixtures\SurveyPortal as TestSurveyPortal;

/**
 * @method static void setAuth($user = null)
 * @method static mixed getAuth()
 * @method static bool canLoadPortalView(?string $path = null)
 * @method static void tokenRoutes()
 * @method static void portalRoutes(?Closure $callback = null)
 * @method static bool isValidReferer(?string $referer)
 * @method static bool isAllowed(string $url)
 * @method static array abilities()
 *
 * @see \Sirs\SurveyPortal\SurveyPortal
 */
class SurveyPortal extends Facade
{
    /**
     * Fake the instance for testing
     *
     * @return \Sirs\SurveyPortal\Tests\Fixtures\SurveyPortal
     */
    public static function fake(): TestSurveyPortal
    {
        static::swap($fake = new TestSurveyPortal);

        return $fake;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'survey-portal';
    }
}
