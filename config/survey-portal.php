<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    'home' => env('SURVEY_PORTAL_HOME'),

    'allowed' => [

        'referers' => explode(',', env('SURVEY_PORTAL_REFERERS', '')),

        'paths' => [],

    ],

    'provider' => [

        'name' => 'portal',

        'options' => [
            'driver' => 'eloquent',
            'model' => \Sirs\SurveyPortal\Models\ApiUser::class,
        ],
    ],

    'abilities' => [

        // 'ability-name' => 'Ability Label',

    ],

];
