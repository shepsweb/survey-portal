<?php

use Illuminate\Support\Arr;

if (!function_exists('portal_url')) {
    /**
     * Create a portal url
     *
     * @param string $path
     * @param array $query
     * @return string
     */
    function portal_url(string $path, array $query = []): string
    {
        $query = Arr::query(array_merge($query, [
            'token' => request()->bearerToken() ?? request()->token,
            'portal' => true
        ]));

        return url("{$path}?{$query}");
    }
}
