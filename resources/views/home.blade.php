@extends('survey-portal::layouts.app')

@section('content')
<div class="vw-100 vh-100 d-flex justify-content-center align-items-center">
    <div class="p-5 text-center">
        <h1 class="border-0">Welcome!</h1>
        <p class="text-secondary">SIRS Survey Portal</p>
    </div>
</div>
@endsection
