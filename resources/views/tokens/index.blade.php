@extends('survey-portal::layouts.app')

@section('content')
<div class="p-5">
    @if(session()->has('access_token'))
    <div class="alert alert-warning" role="alert">
        <h3>
            API token for {{session()->get('email')}}
        </h3>
        <p class="mb-0">{{session()->get('access_token')}}</p>
    </div>
    @endif

    @if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        <p>{{session()->get('success')}}</p>
    </div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
            <li class="{{ $loop->last ? 'mb-0' : '' }}">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="d-flex justify-content-between align-items-center">
        <h2>Api Users</h2>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">Create</button>
    </div>

    <table class="table table-striped mt-3">
        <thead>
            <tr>
                <th scope="col" class="text-left">Email</th>
                <th scope="col" class="text-center">Abilities</th>
                <th scope="col" class="text-center">Created Date</th>
                <th scope="col" class="text-center" style="width: 150px;">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse($users as $user)
            <tr>
                <td class="text-left align-middle">{{ $user->email }}</td>
                <td class="text-center align-middle">{{ $user->abilities() }}</td>
                <td class="text-center align-middle">{{ $user->created_at->diffForHumans() }}</td>
                <td class="text-center align-middle">
                    <form class="d-flex justify-content-center" action="/survey-portal/tokens/{{$user->id}}"
                        method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">Revoke</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" class="text-center text-muted">No records found</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Create Api User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/survey-portal/tokens" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            name="email">
                        <small id="emailHelp" class="form-text text-muted">
                            Helps identify a user's token.
                        </small>
                    </div>
                    <h5 class="mb-0">Abilities</h5>
                    <small class="form-text text-muted mt-0 mb-1">
                        All by default or select specific abilities below.
                    </small>
                    @foreach($abilities as $ability => $label)
                    <div class="form-group form-check mb-0">
                        <input type="checkbox" class="form-check-input" id="{{ $ability }}" name="scopes[]"
                            value="{{ $ability }}">
                        <label class="form-check-label" for="{{ $ability }}">{{ $label }}</label>
                    </div>
                    @endforeach
                    <div class="form-group form-check mb-0">
                        <input type="checkbox" class="form-check-input" id="all" name="scopes[]" value="*">
                        <label class="form-check-label" for="all">All</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
