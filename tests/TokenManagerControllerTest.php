<?php

namespace Sirs\SurveyPortal\Tests;

use Illuminate\Http\Response;
use Sirs\SurveyPortal\Models\ApiUser;
use Sirs\SurveyPortal\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Sirs\SurveyPortal\Facades\SurveyPortal;

class TokenManagerControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_see_the_token_manager_screen()
    {
        SurveyPortal::fake()
            ->setAbility('*', 'All')
            ->setAbility('test', 'Test');

        factory(ApiUser::class, 5)->create();

        $response = $this->get(route('survey-portal.tokens.index'));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertTrue(isset($response->original->getData()['users']));
        $this->assertCount(5, $response->original->getData()['users']);

        $this->assertTrue(isset($response->original->getData()['abilities']));
        $this->assertCount(2, $response->original->getData()['abilities']);
    }

    /** @test */
    public function it_can_create_an_apiuser()
    {
        $payload = [
            'email' => 'joe@example.com',
            'scopes' => ['*'],
        ];

        $response = $this->post(route('survey-portal.tokens.store'), $payload);

        $response->assertStatus(Response::HTTP_FOUND)
            ->assertSessionHas('access_token');

        $this->assertDatabaseHas('api_users', ['email' => $payload['email']]);
        $this->assertDatabaseHas('personal_access_tokens', [
            'tokenable_type' => ApiUser::class,
            'abilities' => json_encode($payload['scopes']),
            'name' => config('app.url'),
        ]);
    }

    /** @test */
    public function it_requires_an_email_to_create_token()
    {
        $payload = [
            'email' => '',
            'scopes' => ['*'],
        ];

        $response = $this->post(route('survey-portal.tokens.store'), $payload);

        $response->assertSessionHasErrors('email')
            ->assertSessionMissing('access_token');
    }

    /** @test */
    public function it_can_revoke_an_apiuser()
    {
        $apiUser = factory(ApiUser::class)->create();
        $token = $apiUser->createToken(config('app.url'), ['*']);

        $response = $this->delete(route('survey-portal.tokens.destroy', ['user' => $apiUser]));

        $response->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseMissing('api_users', $apiUser->toArray());
        $this->assertDatabaseMissing('personal_access_tokens', $token->accessToken->toArray());
    }
}
