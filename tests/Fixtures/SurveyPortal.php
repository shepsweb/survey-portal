<?php

namespace Sirs\SurveyPortal\Tests\Fixtures;

use Sirs\SurveyPortal\SurveyPortal as BaseSurveyPortal;

class SurveyPortal extends BaseSurveyPortal
{
    protected $abilities = [];

    /**
     * Get abilities from config
     *
     * @return array
     */
    public function abilities(): array
    {
        return $this->abilities;
    }

    /**
     * Set an ability
     *
     * @param string $key
     * @param string $value
     * @return self
     */
    public function setAbility(string $key, string $value): self
    {
        $this->abilities[$key] = $value;

        return $this;
    }
}
