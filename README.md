# Survey Portal Package

## Installation

You can install the package via composer:

Add this to the `composer.json`

```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/shepsweb/survey-portal"
    }
]
```

And run

```bash
composer require sirs/survey-portal:dev-master
```

## Usage

```php
// Usage description here
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email example@something.com instead of using the issue tracker.

## Credits

-   [Sheps Team](https://github.com/shepsweb)
-   [All Contributors](../../contributors)

## License

The The Unlicense. Please see [License File](LICENSE.md) for more information.
